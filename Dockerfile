FROM registry.gitlab.com/clecherbauer/docker-images/python:3.6-debian-stretch

COPY .build /.build
RUN /.build/build.sh
