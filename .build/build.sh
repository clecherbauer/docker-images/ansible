#!/bin/bash
# shellcheck disable=SC1091
# shellcheck disable=SC1090
# shellcheck disable=SC2001
set -e

#disable SSH Host-Key Verification for gitlab
echo -e "Host gitlab\n\tStrictHostKeyChecking no\n\tUserKnownHostsFile=/dev/null\n" >> /etc/ssh/ssh_config
echo -e "Host gitlab.comsolit.net\n\tStrictHostKeyChecking no\n\tUserKnownHostsFile=/dev/null\n" >> /etc/ssh/ssh_config

# install ansible dependencies
pip install cryptography===3.3
pip install jmespath ansible

# sshpass is required to provision using ssh-copy-id
# vim is required for ansible-vault
apt update && apt install -y sshpass vim && apt-get clean

if [ ! -d /.ansible ]; then
  mkdir /.ansible
fi
chmod 777 /.ansible